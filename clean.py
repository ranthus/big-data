import pandas as pd
import numpy as np
from petl import cut,tocsv,tojson,tojsonarrays

missing_values = ["n/a", "na", "--"]
clean_data = pd.read_csv('etl.csv', na_values = missing_values)
print('\nSetelah ETL :\n')
print(clean_data)

cnt=0
for row in clean_data['Milik Penduduk']:
    try:
        int(row)
        clean_data.loc[cnt, 'Milik Penduduk']=np.nan
        # clean_data.loc[cnt, 'OWN_OCCUPIED']=clean_data.loc[cnt, 'OWN_OCCUPIED']
    except ValueError:
        # clean_data.loc[cnt, 'OWN_OCCUPIED']=np.nan
        pass
    cnt+=1
# replace string to N/A
cnta=0
for row in clean_data['Jumlah Kamar Mandi']:
    try:
        int(row)
        clean_data.loc[cnta, 'Jumlah Kamar Mandi']=clean_data.loc[cnta, 'Jumlah Kamar Mandi']
    except ValueError:
        clean_data.loc[cnta, 'Jumlah Kamar Mandi']=np.nan
        # pass
    cnta+=1

bedroom = clean_data['Jumlah Kamar'].median()
pid = clean_data['PID'].median()
st_num = clean_data['Nomor Jalan'].median()
bath = clean_data['Jumlah Kamar Mandi'].median()
luas = clean_data['Luas'].median()

clean_data['PID'].fillna(pid, inplace=True)
clean_data['Nomor Jalan'].fillna(st_num, inplace=True)
clean_data['Nama Jalan'].fillna('N/A', inplace=True)
clean_data['Milik Penduduk'].fillna('N', inplace=True)
clean_data['Jumlah Kamar'].fillna(bedroom, inplace=True)
clean_data['Jumlah Kamar Mandi'].fillna(bath, inplace=True)
clean_data['Luas'].fillna(luas, inplace=True)

print('\nFix ETL + Clean Data:\n')
print(clean_data)
# clean_data = json.loads(clean_data)
save = tojsonarrays(clean_data,'Tugas ETL + Clean Data.json')