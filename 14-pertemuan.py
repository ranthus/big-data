import petl as etl
from petl import valuecounter
from petl import cut,tocsv

# data = etl.fromjson('siakad.json')
data = etl.fromjson('gojek2.json')

#Ganti nama row tabel 
# for row in data:
# 	print(row)

# print('\n')

# baru = etl.rename(data,{'NIM':'NIM Mahasiswa'})
# baru = cut(baru,'NIM Mahasiswa')

# for row in baru:
# 	print(row)

# Distinct
# data1 = valuecounter(data,'text') 
# for row in data1:
# 	simpan = print(row)

# Filter
data_filter = etl.search(data,'text','logo')
data_filter = etl.valuecounter(data_filter,'text')
for row in data_filter:
	print(row)