import csv

with open('label2.csv', 'r+') as csv_file:
	csv_reader = csv.reader(csv_file, delimiter=',')
	for row in csv_reader: 
		print(row[10])
		label = input("Tentukan label :")
		with open('label_fix.csv', mode='a') as sentimen:
			sentimen_writer = csv.writer(sentimen, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
			if (label == '9'):
				sentimen_writer.writerow(['Sentimen', row[10]])
			if (label == '1'):
				sentimen_writer.writerow(['Positif', row[10]])
			elif(label == '2'):
				sentimen_writer.writerow(['Negatif', row[10]])
			elif(label == '3'):
				sentimen_writer.writerow(['Netral', row[10]])
			else:
				sentimen_writer.writerow(['', row[10]])