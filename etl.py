import petl as etl
from petl import valuecounter
from petl import cut,tocsv,tojson,tojsonarrays

data = etl.fromcsv('property data.csv')
print('Data Awal :\n')
print(data)

df = etl.rename(data,{'NUM_BATH':'Jumlah Kamar Mandi','ST_NAME':'Nama Jalan','ST_NUM':'Nomor Jalan','OWN_OCCUPIED':'Milik Penduduk','NUM_BEDROOMS':'Jumlah Kamar','SQ_FT':'Luas'})
simpan = tocsv(df,'etl.csv')
print('\nSetelah ETL :\n')
print(df)