# PRINT
# print("Hello World !")


# print("The itsy bitsy spider climbed up the waterspout.")
# print("Down came the rain and washed the spider out.")

# print("The itsy bitsy spider \nclimbed up the waterspout.")
# print()
# print("Down came the rain \nand washed the spider out.")

# print("My", "name", "is", sep="_", end="*")
# print("Monty", "Python.", sep="*", end="*\n")

# TIPE DATA
# a=100
# print("The type of variable having value",a, "is", type(a))

# b=10.2345
# print("The type of variable having value",b, "is", type(b))

# a='budi'
# print("The type of variable having value",a, "is", type(a))
# print("\n")
# nama = "Reno"
# jenis_kelamin = "L"
# alamat = """ Jl. Tritura Gg. Angket Dalam"""
# agama = "Islam"
# print("Nama :",nama,"\n","Jenis Kelamin :",jenis_kelamin,"\n","Alamat :",alamat,"\n","Agama :",agama)


# INPUT
# panjang = input("Masukkan nilai panjang : ")
# lebar = input("Masukkan nilai lebar : ")
# luas = int(panjang) * int(lebar)
# print("Luas = ",luas)

# TUGAS Pertama
# No 1
# import math
# print("Soal nomor 1 :")
# a = input("Masukkan nilai A : ")
# b = input("Masukkan nilai B : ")
# a = int(a)
# b = int(b)
# # a = a**2
# # b = b**2
# c = math.sqrt((a**2)+(b**2))
# print("C = ",c)

# # No 2
# print()
# print("Soal nomor 2 :")
# # a = input("Masukkan niali A : ")
# # b = input("Masukkan niali B : ")
# # a = int(a)
# # b = int(b)
# if a == b:
# 	print("Sama Besar")
# elif a < b:
# 	print("A Lebih Kecil Dari B")
# else :
# 	print("Tidak")


# Struktur Data - List
# list1 = ['sisfo', 'siskom', 2008, 2014]
# list2 = [1,2,3,4,5,6,7,8,9]
# print("list1[0]: ", list1[0])
# print("list2[1:5]: ", list2[1:5])

# List - Update List
# list1 = ['phsycs', 'chemistry', 1997, 2000]
# print("Value available at index 2 :")
# print(list1[2])
# list1[2] = 2001
# print("New Value available at index 2 :")
# print(list1[2])


# List - Delet List Element
# list1 = ['phsycs', 'chemistry', 1997, 2000]
# print("Value available at index 2 :")
# print(list1)
# del list1[2]
# print("After deleting value at index 2 :")
# print(list1)

# List - Adding List
# Append
# buah.append("manggis")
# buah = ["jeruk","apel","mangga","duren"]
# print(buah)
# Prepend
# buah = ["jeruk","apel","mangga","duren"]
# buah.prepend("anggur") // fungsi prepend sudah gak ad
# buah.insert(0,"anggur")
# print(buah)

# # Insert
# buah.insert(2,"semangka")
# print(buah)

# List - Mutli Dimensi
# minuman = [
# 		["Kopi", "Susu", "Teh"],
# 		["Jus Apel", "Jus Melon", "Jus Jeruk"],
# 		["Es Kopi", "Es Campur", "Es Teles"]
# 	]
# print (minuman[2][0])

# List - Contoh Program

# aList = []
# for number in range(1,11):
# 	aList += [number]
# print("The value of aList is :",aList)
# print("\nAccesing values by iteration :")
# for item in aList:
# 	print(item),
# print()
# print("\nAccesing values by index:")
# print("Subscript Value")
# for i in range(len(aList)):
	# print("%9d %7d" % (i, aList[i]))

# print("\nModifyng a list value ...")
# print("Value of aList before modification:", aList)
# aList[0] = 100
# aList[-3] = 19
# print("Value of aList after modification:", aList)

# Lab
angka = []
print("Enter 10 integers:")
angka += [input("Enter integer %1d: "%(i+1)) for i in range (0,10)]
# print(angka)
print("Creating a histogram from values :")
print("Element\tValue\tHistogram")
for i in range(len(angka)):
	angka[i] = int (angka[i])
	print(i,"\t",angka[i],"\t","*"*angka[i])

























