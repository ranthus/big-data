# Importing libraries
import pandas as pd
import numpy as np
import petl as etl
import json
from petl import valuecounter
from petl import cut,tocsv,tojson,tojsonarrays


data = etl.fromcsv('property data.csv')
print('Data Awal :\n')
print(data)
# df = pd.read_csv('property data.csv', na_values = missing_values)
# ETL
df = etl.rename(data,{'NUM_BATH':'Jumlah Kamar Mandi','ST_NAME':'Nama Jalan','ST_NUM':'Nomor Jalan','OWN_OCCUPIED':'Milik Penduduk','NUM_BEDROOMS':'Jumlah Kamar','SQ_FT':'Luas'})
simpan = tocsv(df,'etl.csv')
# End ETL

# Clean Data
missing_values = ["n/a", "na", "--"]
clean_data = pd.read_csv('etl.csv', na_values = missing_values)
print('\nSetelah ETL :\n')
print(clean_data)
# print(df)



# Read csv file into a pandas dataframe
# # df = pd.read_csv("property data.csv")
# # dict
# # numbers = [1,2,3,4,5,6,7,8,9,0]
# # Take a look at the first few rows
# print(df1.head())

# Looking at the ST_NUM column
# print (df['ST_NUM'])
# print (df['ST_NUM'].isnull())

# Looking at the NUM_BEDROOMS column
# print (df['NUM_BEDROOMS'])
# print (df['NUM_BEDROOMS'].isnull())

# Looking at the OWN_OCCUPIED column
# df['OWN_OCCUPIED'] = np.dtype(str)
# print (df['OWN_OCCUPIED'])
# print (df['OWN_OCCUPIED'].isnull())

# Detecting numbers 
# replace number to N/A
cnt=0
for row in clean_data['Milik Penduduk']:
    try:
        int(row)
        clean_data.loc[cnt, 'Milik Penduduk']=np.nan
        # clean_data.loc[cnt, 'OWN_OCCUPIED']=clean_data.loc[cnt, 'OWN_OCCUPIED']
    except ValueError:
        # clean_data.loc[cnt, 'OWN_OCCUPIED']=np.nan
        pass
    cnt+=1
# replace string to N/A
cnta=0
for row in clean_data['Jumlah Kamar Mandi']:
    try:
        int(row)
        clean_data.loc[cnta, 'Jumlah Kamar Mandi']=clean_data.loc[cnta, 'Jumlah Kamar Mandi']
    except ValueError:
        clean_data.loc[cnta, 'Jumlah Kamar Mandi']=np.nan
        # pass
    cnta+=1

# # Total missing values for each feature
# # print(clean_data.isnull().sum())

# # Any missing values?
# # print (clean_data.isnull().values.any())

# # Total number of missing values
# # print (clean_data.isnull().sum().sum())

# # Replace using median 
bedroom = clean_data['Jumlah Kamar'].median()
pid = clean_data['PID'].median()
st_num = clean_data['Nomor Jalan'].median()
bath = clean_data['Jumlah Kamar Mandi'].median()
luas = clean_data['Luas'].median()


# # # Replace missing values (N/A) with a number
clean_data['PID'].fillna(pid, inplace=True)
clean_data['Nomor Jalan'].fillna(st_num, inplace=True)
clean_data['Nama Jalan'].fillna('N/A', inplace=True)
clean_data['Milik Penduduk'].fillna('N', inplace=True)
clean_data['Jumlah Kamar'].fillna(bedroom, inplace=True)
clean_data['Jumlah Kamar Mandi'].fillna(bath, inplace=True)
clean_data['Luas'].fillna(luas, inplace=True)

print('\nFix ETL + Clean Data:\n')
print(clean_data)
# clean_data = json.loads(clean_data)
save = clean_data.to_json('Tugas ETL + Clean Data.json')
# End Clean Data
# Location based replacement
# clean_data.loc[2,'ST_NUM'] = 125

# Replace using median 
# median = df['NUM_BEDROOMS'].median()
# df['NUM_BEDROOMS'].fillna(median, inplace=True)

# for bag in clean_data.groupby(["Nomor Jalan", "Nama Jalan"]):
#     contents_df = bag.drop(["Nomor Jalan", "Nama Jalan"], axis=1)
#     subset = [OrderedDict(row) for i,row in contents_df.iterrows()]
#     results.append(OrderedDict([("Nomor Jalan", zipcode),
#                                 ("Nama Jalan", state),
#                                 ("subset", subset)]))

# print (json.dumps(results[0], indent=4))
#with open('ExpectedJsonFile.json', 'w') as outfile:
#    outfile.write(json.dumps(results[0], indent=4))
