from string import punctuation
from nltk.tokenize import word_tokenize
from nltk.util import ngrams
from collections import Counter
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
factory = StemmerFactory()
stemmer = factory.create_stemmer()
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory
factory = StopWordRemoverFactory()
stopwords = factory.get_stop_words()
import json, re, nltk, string, csv
import pandas as pd
import vincent

def hapus_tanda(tanda):
    tanda = tanda.replace("\\n","").replace('"', ' ').replace(",", '').replace(";", '').replace(":", '').replace("\\N","").replace("'", ' ').replace("[","").replace("]","").replace("."," ").replace("-"," ").replace("?"," ").replace("!"," ").replace("="," ")
    return tanda

def remove_before_tokens(sentence):
    sentence = sentence.strip()
    PATTERN = r'[^a-zA-Z0-9]'
    filtered_sentence = re.sub(PATTERN, r' ', sentence)
    return(filtered_sentence)

def removeStemmer_before_tokens(text):
    tokens = nltk.word_tokenize(text)
    return [stemmer.stem(w) for w in tokens]

def getStopWordList(stopWordListFileName):
#read the stopwords file and build a list
    stopWords = []
 
    fp = open(stopWordListFileName, 'r')
    line = fp.readline()
    while line:
        word = line.strip()
        stopWords.append(word)
        line = fp.readline()
    fp.close()
    return stopWords

st = open('stopword.txt', 'r')
stopWords = getStopWordList('stopword.txt')

 
def tokenize(s):
    return tokens_re.findall(s)
 
def preprocessing(content):
    sentences = content.split()
    cleaned_sentences = []
    
    for s in sentences :
        s = s.lower()
        s = re.sub('((www\.[^\s]+)|(https?://[^\s]+))','URL',s)
        #remove username
        s = re.sub('@[^\s]+','AT_USER',s)
        #remove hashtag
        s = re.sub(r'#([^\s]+)',r'\1',s)
        s = hapus_tanda(s)
        s = remove_before_tokens(s)
        s = ' '.join(removeStemmer_before_tokens(s))
        val = re.search(r"^[a-zA-Z][a-zA-Z0-9]*$", s)

        if(s in stopWords or val is None):
            continue           
        else:
            cleaned_sentences.append(s.lower())
        print ('\t proses stopword === ', s)
    
    return cleaned_sentences

def get_non_empty_lines(filename):
    with open(filename) as fp:
        for line in fp:
            stripped_line = line.strip()
            if stripped_line:
                yield stripped_line

simpan = []
count_all = Counter()
for line in get_non_empty_lines('cbcraw.json'):
    tweets = json.loads(line)
    preprocess = preprocessing(tweets["text"])
    print('\t proses preprocessing === ', preprocess)
    #hasil1=' '.join(str(v) for v in preprocess)
    count_all.update(preprocess)
    word_freq = count_all.most_common(5)
    labels, freq = zip(*word_freq)
    data = {'data': freq, 'x': labels}
    bar = vincent.Bar(data, iter_idx='x')
    bar.to_json('term_freq.json', html_out=True, html_path='chart.html')

    simpan.append(preprocess)
