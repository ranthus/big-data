from nltk.tokenize import word_tokenize
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
factory = StemmerFactory()
stemmer = factory.create_stemmer()
from Sastrawi.StopWordRemover.StopWordRemoverFactory import StopWordRemoverFactory
factory = StopWordRemoverFactory()
stopwords = factory.get_stop_words()
import json, re, nltk, string, csv
import pandas as pd
import vincent

def hapus_tanda(tanda):
    tanda = tanda.replace("\\n","").replace('"', ' ').replace(",", '').replace(";", '').replace(":", '').replace("\\N","").replace("'", ' ').replace("[","").replace("]","").replace("."," ").replace("-"," ").replace("?"," ").replace("!"," ").replace("="," ")
    return tanda

def remove_before_tokens(sentence):
    sentence = sentence.strip()
    PATTERN = r'[^a-zA-Z0-9]'
    filtered_sentence = re.sub(PATTERN, r' ', sentence)
    return(filtered_sentence)

def removeStemmer_before_tokens(text):
    tokens = nltk.word_tokenize(text)
    return [stemmer.stem(w) for w in tokens]

def getStopWordList(stopWordListFileName):
#read the stopwords file and build a list
    stopWords = []
 
    fp = open(stopWordListFileName, 'r')
    line = fp.readline()
    while line:
        word = line.strip()
        stopWords.append(word)
        line = fp.readline()
    fp.close()
    return stopWords

st = open('stopword.txt', 'r')
stopWords = getStopWordList('stopword.txt')

def preprocessing(content):
    sentences = content.split()
    cleaned_sentences = []
    
    for s in sentences :
        s = s.lower()
        s = re.sub('((www\.[^\s]+)|(https?://[^\s]+))','URL',s)
        #remove username
        s = re.sub('@[^\s]+','AT_USER',s)
        #remove hashtag
        s = re.sub(r'#([^\s]+)',r'\1',s)
        s = hapus_tanda(s)
        s = remove_before_tokens(s)
        s = ' '.join(removeStemmer_before_tokens(s))
        val = re.search(r"^[a-zA-Z][a-zA-Z0-9]*$", s)

        if(s in stopWords or val is None):
            continue           
        else:
            cleaned_sentences.append(s.lower())
        #print ('\t proses stopword === ', s)
    
    return cleaned_sentences

def get_non_empty_lines(filename):
    with open(filename) as fp:
        for line in fp:
            stripped_line = line.strip()
            if stripped_line:
                yield stripped_line
                
dates_ITAvWAL = []
# f is the file pointer to the JSON data set
for line in get_non_empty_lines('cbcraw.json'):
    tweet = json.loads(line)
    # let's focus on hashtags only at the moment
    terms_hash = [term for term in preprocessing(tweet['text']) if term.startswith('#')]
    # track when the hashtag is mentioned
    if '#itavwal' in terms_hash:
        dates_ITAvWAL.append(tweet['created_at'])
 
# a list of "1" to count the hashtags
# a list of "1" to count the hashtags
ones = [1]*len(dates_ITAvWAL)
# the index of the series
idx = pd.DatetimeIndex(dates_ITAvWAL)
# the actual series (at series of 1s for the moment)
ITAvWAL = pd.Series(ones, index=idx)
 
# Resampling / bucketing
per_minute = ITAvWAL.resample('1Min', how='sum').fillna(0)
time_chart = vincent.Line(ITAvWAL)
time_chart.axis_titles(x='Time', y='Freq')
time_chart.to_json('time_chart.json')
