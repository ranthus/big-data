import json
import tweepy
from textblob import TextBlob
import pandas as pd 
import matplotlib.pyplot as plt 

fname2 = 'fix_gojek.json'
post = 0
neg = 0
with open(fname2, 'r') as f2:
	for line2 in f2:
		tweet = json.loads(line2)
		# print(tweet['text'])
		analysis = TextBlob(tweet['text'])
		if analysis.sentiment[0]>0:
			print ('Positive')
			post += 1
		else:
			neg = neg + 1
			print ('Negative')

import vincent
word_freq = [post,neg]
plt.pie(word_freq, labels = {"Negative", "Positive"},
autopct ='% 1.1f %%', shadow = True) 

pie = vincent.Pie(word_freq)
pie.legend('Negative','Positive')
pie.to_json('sentimen.json')
plt.show()