from sqlalchemy import create_engine
from pandas.io import sql
import pandas as pd

json = pd.read_json('siakad.json')
engine = create_engine('sqlite:///:memory:')
json.to_sql('data_json',engine)

res_json = pd.read_sql_query('select * from data_json',engine)
print(json)