a=100
print("The type of variable having value",a, "is", type(a))

b=10.2345
print("The type of variable having value",b, "is", type(b))

c=100+3j
print("The type of variable having value",c, "is", type(c))
print("\n")
nama = "Reno"
jenis_kelamin = "L"
alamat = """ Jl. Tritura Gg. Angket Dalam"""
agama = "Islam"

print("Nama :",nama,"\n","Jenis Kelamin :",jenis_kelamin,"\n","Alamat :",alamat,"\n","Agama :",agama)