# import pandas as pd
# a = pd.Series()
# print(a)

# Python Pandas - Membuat series
# import pandas as pd
# import numpy as np
# data = np.array(['a','b','c','d'])
# s = pd.Series(data)
# s = pd.Series(data,index=[1,2,3,4])
# print(s)

# Python Pandas - Membuat DataFrame
import pandas as pd
import numpy as np
data = ['a','b','c','d']
data = [['Reno',10],['Anthus',20]]
s = pd.DataFrame(data, index=['Name','Age'],columns=['Name','Age'])
s = pd.DataFrame(data,index=[1,2,3,4])
print(s)

# import pandas as pd
# import numpy as np
data = ['a','b','c','d']
data = [['Reno',10],['Anthus',20]]
s = pd.DataFrame(data, index=['Name','Age'],columns=['Name','Age'])
s = pd.DataFrame(data,index=[1,2,3,4])
print(s)

data = {'Name':['Reno','Anthus'],'Age':[17,18]}
s = pd.DataFrame(data, index=['Name','Age'],columns=['Name','Age'])
s = pd.DataFrame(data,index=[1,2,3,4])
print(s)

#Python Pandas - Column Addition
# import pandas as pd
# import numpy as np
# data = {'one':pd.Series([1,2,3],index=['a','b','c']),'two':pd.Series([2,3,4,5],index=['a','b','c','d'])}
# s = pd.DataFrame(data)
# print('Adding new column by passing as series')
# s['three'] = pd.Series([10,20,30],index=['a','b','c'])

# print('Adding new column using the existing columns in DataFrame:')
# s['four'] = s['one']+s['three']

# print(s)

# data = [['Reno',80],['Anthus',76]]
# df = pd.DataFrame(data,index=[1,2],columns=['Nama','Ujian'])
# df2 = pd.DataFrame([['Ani',90]],index=[3],columns=['Nama','Ujian'])
# df['Tugas'] = pd.Series([90,80],index=[1,2])
# df['Nilai'] = (df['Ujian']+df['Tugas'])/2

# print(df)

# try:
# 	f = open("asd.py",'a')
# 	f.write('\nwkwkwkw')
# 	f.write('\nwkwkwkw')
# 	f.write('\nwkwkwkw')
# 	f.write('\nwkwkwkw')
# 	f.write('\nwkwkwkw')
# 	f.write('\nwkwkwkw')
# finally:
# 	f.close()







