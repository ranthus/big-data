#Read XLSX
from sqlalchemy import create_engine
from pandas.io import sql
import pandas as pd
# data = pd.read_excel('xample.xlsx')
# print(data)
# with pd.ExcelFile('xample.xlsx') as xlsx:
# 	df1 = pd.read_excel(xlsx,'Sheet1')
# 	df2 = pd.read_excel(xlsx,'Sheet2')
# print('Result data sheet1')
# print(df1)
# print('Result data sheet2')
# print(df2)

#Read CSV
# data1 = pd.read_csv('xample.csv')
# print(data1)

#Read Json
# data2 = pd.read_json('xample.json')
# print(data2)

#Python - Tabel Relasional

csv = pd.read_csv('siakad.csv')
json = pd.read_json('siakad.json')
xlsx = pd.read_excel('siakad.xlsx')

#create the db engine
engine = create_engine('sqlite:///:memory:')

#store data frame
csv.to_sql('data_csv',engine)
json.to_sql('data_json',engine)
xlsx.to_sql('data_xlsx',engine)

#data awal
res_csv = pd.read_sql_query('select * from data_csv',engine)
res_json = pd.read_sql_query('select * from data_json',engine)
res_xlsx = pd.read_sql_query('select * from data_xlsx',engine)
print('\t\t\t\t\t\t-------- Data Awal --------')
print('Data CSV :\n',res_csv)
print('\nData JSON :\n',res_json)
print('\nData XLSX :\n',res_xlsx)
print('\n\t\t\t\t\t-------- End Data Awal --------\n')

#insert data
#csv
print('\t\t\t\t\t\t-------- Menambah Data --------')
sql.execute('INSERT INTO data_csv VALUES(?,?,?,?,?,?,?,?,?,?,?,?)', engine, params=[(8,9,'KSI-132','Struktur Data',4,13,100.00,100.00,70.00,88.00,'A',4)])
res_csv = pd.read_sql_query('select * from data_csv',engine)
print('\nData CSV Setelah Ditambah :\n',res_csv)


#json
sql.execute('INSERT INTO data_json VALUES(?,?,?,?,?,?,?,?,?,?,?,?)',engine,params=[(8,13,'KSI-132',4,9,'Struktur Data','A',4,100.00,100.00,70.00,88.00)])
res_json = pd.read_sql_query('select * from data_json',engine)
print('\nData JSON Setelah Ditambah :\n',res_json)

#xlsx
sql.execute('INSERT INTO data_xlsx VALUES(?,?,?,?,?,?,?,?,?,?,?,?)',engine,params=[(8,9,'KSI-132','Struktur Data',4,13,100.00,100.00,70.00,88.00,'A',4)])
res_xlsx = pd.read_sql_query('select * from data_xlsx',engine)
print('\nData XLSX Setelah Ditambah :\n',res_xlsx)
print('\n\t\t\t\t\t-------- End Menambah Data --------\n')

#delete data
#csv
print('\t\t\t\t\t\t-------- Menghapus Data --------')
sql.execute('DELETE FROM data_csv WHERE NIM=(?)',engine,params=[(9)])
res_csv = pd.read_sql_query('select * from data_csv',engine)
print('\nData CSV Setelah Dihapus :\n',res_csv)


#json
sql.execute('DELETE FROM data_json WHERE NIM=(?)',engine,params=[(9)])
res_json = pd.read_sql_query('select * from data_json',engine)
print('\nData JSON Setelah Dihapus :\n',res_json)

#xlsx
sql.execute('DELETE FROM data_xlsx WHERE NIM=(?)',engine,params=[(9)])
res_xlsx = pd.read_sql_query('select * from data_xlsx',engine)
print('\nData XLSX Setelah Dihapus :\n',res_xlsx)
print('\n\t\t\t\t\t-------- End Menghapus Data --------\n')

