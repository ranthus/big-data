# Fungsi
# def sapa(nama):
# 	print("Hi,"+nama+". Apa kabar?")
# nama = input("Masukkan nama :")
# sapa(nama)

# # # Fungsi - Pass by reference value
# def changeme(mylist):
# 	mylist.append([1,2,3,4,5]);
# 	print("Values inside the function: ",mylist)
# 	return
# mylist = [23,43,54];
# changeme(mylist);
# print("Values outside the funtion: ",mylist)

# # Fungsi - Required arguments
# def printme(str):
# 	print(str)
# 	return
# printme("Testing")

# # Fungsi - Keyword arguments
# def printinfo(name,age):
# 	print("Name :",name)
# 	print("Age :",age)
# 	return
# printinfo(name="Reno", age=17)

# # Fungsi - Default argument
# def printinfo(name, age=17):
# 	print("Name :",name)
# 	print("Age :",age)
# 	return
# printinfo(name="Reno", age=15)
# printinfo(name="Reno")

# # Fungsi - Variable-length arguments
# def printinfo(arg1,*vartuple):
# 	print("Output is:")
# 	print(arg1)
# 	for var in vartuple:
# 		print(var)
# 	return
# printinfo(10)
# printinfo(70,60,30)

# # Fungsi - Global vs Local variables
# total = 0
# def jumlah(arg1,arg2):
# 	total = arg1+arg2
# 	print("Inside the function local total:",total)
# 	return total
# jumlah(10,20)
# print("Outside the function global total:",total)

# # Python - Decision making
# var = 100
# if (var == 100):
# 	print("Value of expression is 100")
# print("Good bye !")

# # Python - Perulangan For
# numbers = [7,6,4,3,5,6,7]
# sum = 0
# for each in numbers:
# 	sum = sum + each
# print("Jumlah semuanya :", sum)

# # Python - Perulangan While
# count = 0
# while (count<5):
# 	print("The count is:",count)
# 	count = count + 1
# print("Good bye")

# # Python - Kendali Looping
# for letter in "Programming":
# 	if letter == "g":
# 		break
# 	print("Huruf sekarang:", letter)
# print("Good bye !")

# # Pyton - While else
# count = 0
# while (count<5):
# 	print(count, "Kurang dari 5")
# 	count = count + 1
# else:
# 	print(count, "Tidak kurang dari 5")


# LAB 1
# Soal : Buatlah program untuk mencari rata-rata dari nilai tugas-tugas setiap siswa, serta mencari nilai terkecil dan nilai terbesar dari nilai keseluruhan
import statistics
siswa = []
jumlah_siswa = int(input("Masukkan jumlah siswa :"))
jumlah_mapel = int(input("Masukkan jumlah mata pelajaran :"))
nilai = [[] for z in range(jumlah_siswa)]
for x in range(jumlah_siswa):
	data = str(input("Masukkan nama siswa ke-%1d: "%(x+1)))
	siswa.append(data)
	nilai[x] += [int(input("Masukkan nilai ke-%1d: "%(i+1)))for i in range(jumlah_mapel)]
for i in range(len(siswa)):
	print("\nNama siswa \t:",siswa[i],"\nNilai rata-rata\t:",statistics.mean(nilai[i]),"\nNilai terkecil \t:",min(nilai[i]),"\nNilai terbesar \t:",max(nilai[i]))