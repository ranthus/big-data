import tweepy
import json
from tweepy import OAuthHandler
from tweepy import Stream
from tweepy.streaming import StreamListener

import operator 
from collections import Counter

from nltk.corpus import stopwords
import string


import petl as etl
from petl import valuecounter
from petl import cut,tocsv

import pandas as pd 
import matplotlib.pyplot as plt 


# CRAWLING DATA TWITTER
#AUTH
consumer_key = 'JG8zrSAEMDyl5MISDJ11qui7g'
consumer_secret = 'OkcIl8gGXH98D5gHn8dailGncXgyyHZVNYxunyl2zWDJMmNtL4'
access_token = '123197605-uq82kTlNqCtT4dSw9h76xpoVvdqS4CQwaQD623U5'
access_secret = 'jLiiRL1Zh8a83qd54nxUTu7PHFg2GKZhsztTziJ1VoUWr'
bearer_toker = 'AAAAAAAAAAAAAAAAAAAAAC05%2FQAAAAAAJSQkdwtYGYBrtpyRBTF%2FNYQUZDU%3Dsw7KLX8R3deuzjrNedDcU0eOYEhcUdla1rCa9S3ReJvqgv2IAf'
 
auth = OAuthHandler(consumer_key, consumer_secret)
auth.set_access_token(access_token, access_secret)
 
api = tweepy.API(auth)

def process_or_store(tweet):
    print(json.dumps(tweet))

# curl data
class MyListener(StreamListener):
 
    def on_data(self, data):
        try:
            with open('testing.json', 'a') as f:
                f.write(data)
                return True
        except BaseException as e:
            print("Error on_data: %s" % str(e))
        return True
 
    def on_error(self, status):
        print(status)
        return True
 
twitter_stream = Stream(auth, MyListener())
twitter_stream.filter(track=['#PastiAdaJalan','#Gojek'])

# PREPROCESSING TEXT
import re

# IDENTIFIKASI EMOTICON
emoticons_str = r"""
	(?:
		[:=;] # Eyes
		[oO\-]? # Nose (optional)
		[D\)\]\(\]/\\OpP] # Mouth
	)"""
regex_str = [
	emoticons_str,
	r'<[^>]+>', # HTML tags
	r'(?:@[\w_]+)', # @-mentions
	r"(?:\#+[\w_]+[\w\'_\-]*[\w_]+)", # hash-tags
	r'http[s]?://(?:[a-z]|[0-9]|[$-_@.&amp;+]|[!*\(\),]|(?:%[0-9a-f][0-9a-f]))+', # URLs
 
	r'(?:(?:\d+,?)+(?:\.?\d+)?)', # numbers
	r"(?:[a-z][a-z'\-_]+[a-z])", # words with - and '
	r'(?:[\w_]+)', # other words
	r'(?:\S)', # anything else
	r'(?:[[\w_]+)', # removes '['
]

tokens_re = re.compile(r'('+'|'.join(regex_str)+')', re.VERBOSE | re.IGNORECASE)
emoticon_re = re.compile(r'^'+emoticons_str+'$', re.VERBOSE | re.IGNORECASE)
 
def tokenize(s):
	return tokens_re.findall(s)
 
def preprocess(s, lowercase=False):
	tokens = tokenize(s)
	if lowercase:
		tokens = [token if emoticon_re.search(token) else token.lower() for token in tokens]
	return tokens


# MENGAMBIL DATA 
fname = 'aafix_gojek.json'
with open(fname, 'r') as f:
	count_all = Counter()
	for line in f: 
		tweet = json.loads(line)
		punctuation = list(string.punctuation)
		# STOP WORD
		# stop = stopwords.words('english') + punctuation + ['rt','RT', 'via']
		stop = stopwords.words('english') + punctuation + ['rt','RT', 'via','·',':','…','2','Mereka','yang','https://t.co/WQRFk1zTII','ads','ni','https://t.co/rtkJL0DXBz','di','ke','@zulkiflab','🏻','#','@menyangga','yg','aku','dan','2019','3019','gojek','GOJEK','memang','ini','ada','AKU']
		terms_stop = [term for term in preprocess(tweet['text']) if term not in stop]
		count_all.update(terms_stop)
	print(count_all.most_common(13))
# MEMBUAT BAR CHART
import vincent
 
word_freq = count_all.most_common(13)
labels, freq = zip(*word_freq)
data = {'data': freq, 'x': labels}
bar = vincent.Bar(data, iter_idx='x')
bar.to_json('BarGojek.json')


import pandas
import json
 
waktu_gojek = []
# MENGAMBIL DATA
fname2 = 'aafix_gojek.json'
with open(fname2, 'r') as f2:
	for line2 in f2:
		tweet = json.loads(line2)
		# MENTRACKING WAKTU MENTION AKUN GOJEK 
		terms_hash = [term for term in preprocess(tweet['text']) if term.startswith('@')]
		if '@gojekindonesia' in terms_hash:
			waktu_gojek.append(tweet['created_at'])
 
# a list of "1" to count the hashtags
ones = [1]*len(waktu_gojek)
# the index of the series
idx = pandas.DatetimeIndex(waktu_gojek)
# the actual series (at series of 1s for the moment)
gojek = pandas.Series(ones, index=idx)
 
# Resampling / bucketing per 20 menit
minute = gojek.resample('20Min', how='sum').fillna(0)

# MEMBUAT TIME CHART
time_chart = vincent.Line(minute)
time_chart.axis_titles(x='Time', y='Freq')
time_chart.to_json('TimeGojek.json')


# GEO LOCATION
with open(fname, 'r') as f3:
	geo_data = {
		"type": "FeatureCollection",
		"features": []
	}
	for line in f3:
		tweet = json.loads(line)
		if tweet['coordinates']:
			geo_json_feature = {
				"type": "Feature",
				"geometry": tweet['coordinates'],
				"properties": {
					"text": tweet['text'],
					"created_at": tweet['created_at']
				}
			}
			geo_data['features'].append(geo_json_feature)
 
# MEMBUAT JSON GEO LOCATION
with open('GeoGojek.json', 'w') as fout:
	fout.write(json.dumps(geo_data, indent=4))