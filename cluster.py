import petl as etl
import pandas as pd
import json
from petl import valuecounter
from petl import cut,tocsv
import matplotlib.pyplot as plt

# MENGAMBIL DATA 
data = pd.read_json('fix_gojek.json',lines=True)

# FILTER DATA
df = pd.DataFrame(data.loc[0::,['source']])

# MENGHAPUS TAG HTML PADA DATA
df.source = df.source.str.extract('>(.+?)<', expand=False).str.strip() 

# MEMBUAT BAR CHART
df.source.value_counts(normalize=True).plot(kind='bar', grid=True, figsize=(30, 20)).set_xlim(right=2.5)

# MENAMPILKAN BAR CHART
plt.show()
